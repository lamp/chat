import mongoose from "mongoose";
import server from "./server.js";
import "./socketio.js";

await mongoose.connect(process.env.MONGODB_URI || "mongodb://127.0.0.1:27017/chatserver");
server.listen(process.env.PORT || 8535, process.env.ADDRESS);
