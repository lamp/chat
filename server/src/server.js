import { createServer } from "http";
import app from "./app.js";

export default createServer(app);
