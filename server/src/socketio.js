import { Server as SocketIOServer } from "socket.io";
import { randomUUID } from "crypto";
import server from "./server.js";
import Message from "./models/Message.js";

export var io = new SocketIOServer(server, {
	cors: {origin: "*"},
	maxHttpBufferSize: 100000,
	perMessageDeflate: true
});

io.on("connection", socket => {
	socket.ip = socket.handshake.headers["x-forwarded-for"]?.split(',')[0] || socket.handshake.address;
	console.debug("connection from", socket.ip, "socket id", socket.id);
	socket.on("user", user => {
		user = {
			name: user.name?.trim()?.substring(0,32),
			color: user.color?.trim()?.substring(0,32),
			website: user.website?.trim()?.substring(0,1000),
			uuid: user.uuid?.substring(0,128) || "A"+randomUUID(),
			secret: user.secret?.substring(0,128),
			socketid: socket.id,
			ip: socket.ip,
			agent: socket.handshake.headers["user-agent"]
		};
		console.debug("user", user);
		socket.data.user = user;
		broadcastUsers();
	});
	socket.once("user", async user => {
		//await newMessage({color: "#00FF00", content:`${user.name} connected`});
		socket.on("disconnect", () => {
			//newMessage({color: "#FF0000", content: `${socket.data.user.name} disconnected`});
			broadcastUsers();
		});
		socket.on("message", message => {
			newMessage({
				content: message.content,
				user: {...socket.data.user}
			}).catch(error => {
				console.error(socket.id, error.message);
			});
		});
		socket.on("type", () => {
			io.emit("type", socket.id);
		});
		socket.on("mouse", (x, y) => {
			//socket.broadcast.emit("mouse", x, y, socket.id);
			// see own cursor (test)
			io.emit("mouse", x, y, socket.id);
		});
		Message.getMessages({secret: user.secret}).then(messages => {
			socket.emit("messages", messages);
		});
	});
});

async function broadcastUsers() {
	var users = await io.fetchSockets().then(sockets => sockets.filter(socket => socket.data.user).map(socket => {
		var user = Object.assign({}, socket.data.user);
		delete user.ip;
		delete user.secret;
		return user;
	}));
	io.emit("users", users);
}

async function newMessage(message) {
	message = new Message(message);
	message.timestamp = new Date();
	message = await message.save();
	message = message.toObject();
	console.debug("message", message);
	delete message.user.ip;
	delete message.user.secret;
	io.emit("message", message);
}