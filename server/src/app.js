import express from "express";
import {router} from "./router.js";

var app = express();

app.set("trust proxy", true);

app.use((req, res, next) => {
	console.log(req.ip, req.method, req.url, req.headers["user-agent"]);
	next();
});
app.use((req, res, next) => {
	res.header("Access-Control-Allow-Origin", "*");
	res.header("Access-Control-Allow-Headers", "*");
	res.header("Access-Control-Allow-Methods", "*");
	next();
});

app.use(router);

export default app;