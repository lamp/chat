import {Schema, model} from "mongoose";

var schema = new Schema({
	name: {
		type: String,
		unique: true,
		match: /^[a-z0-9_-]+$/i,
		required: true
	},
	type: {
		type: String,
		enum: ["image/png", "image/jpeg", "image/jpg", "image/webp", "image/gif"],
		required: true
	},
	data: {
		type: Buffer,
		required: true
	}
});

export default model("Emoji", schema);