import { Schema, model } from "mongoose";

var schema = new Schema({
	timestamp: Date,
	content: String,
	user: {
		name: String,
		color: String,
		website: String,
		uuid: String,
		secret: {type: String, select: false},
		ip: {type: String, select: false},
		agent: String
	}
}, {
	strict: true,
	strictQuery: true
});


schema.static("getMessages", async function({query = {}, secret}) {
	var messages = await this.find(query).sort({timestamp: -1}).limit(100).select('+user.secret').lean().exec();
	for (var message of messages) {
		if (message.user) {
			if (secret && secret === message.user.secret) {
				message.user.you = true;
			}
			delete message.user.secret;
		}
	}
	return messages.reverse();
});


export default model("Message", schema);

