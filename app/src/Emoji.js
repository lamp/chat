import { useState } from 'react';
import { SERVER_BASE_URL } from './App';

export function Emoji({ emoji }) {
	var [failed, setFailed] = useState(null);
	if (failed) return `:${emoji}:`;
	else return <img src={SERVER_BASE_URL + "/emoji/" + emoji} alt={`:${emoji}:`} title={emoji} className="inline-block w-8 h-8" onError={() => setFailed(true)} />;
}
